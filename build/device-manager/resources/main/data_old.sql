INSERT INTO public.device_models (id, vendor, model)
VALUES (1, 'ELTEX', 'ESR-10')
ON CONFLICT DO NOTHING;
INSERT INTO public.device_models (id, vendor, model)
VALUES (2, 'ELTEX', 'ESR-12')
ON CONFLICT DO NOTHING;
INSERT INTO public.device_models (id, vendor, model)
VALUES (3, 'ELTEX', 'ESR-100')
ON CONFLICT DO NOTHING;
INSERT INTO public.device_models (id, vendor, model)
VALUES (4, 'ELTEX', 'ESR-1000')
ON CONFLICT DO NOTHING;
INSERT INTO public.device_models (id, vendor, model)
VALUES (5, 'Cisco', 'ISR 4000')
ON CONFLICT DO NOTHING;
INSERT INTO public.device_models (id, vendor, model)
VALUES (6, 'Cisco', 'NCS 5000')
ON CONFLICT DO NOTHING;
-- SELECT setval('device_models_id_seq', 100, false); -- next ID value is 100

INSERT INTO public.groups (id, name)
VALUES (1, 'Russia')
ON CONFLICT DO NOTHING;
INSERT INTO public.groups (id, name)
VALUES (2, 'DV')
ON CONFLICT DO NOTHING;
INSERT INTO public.groups (id, name)
VALUES (3, 'Sibir')
ON CONFLICT DO NOTHING;
INSERT INTO public.groups (id, name)
VALUES (4, 'Center')
ON CONFLICT DO NOTHING;
INSERT INTO public.groups (id, name)
VALUES (5, 'NSO')
ON CONFLICT DO NOTHING;
INSERT INTO public.groups (id, name)
VALUES (6, 'Nsk')
ON CONFLICT DO NOTHING;
INSERT INTO public.groups (id, name)
VALUES (7, 'Zael''covskiy')
ON CONFLICT DO NOTHING;
INSERT INTO public.groups (id, name)
VALUES (8, 'Sovetskiy')
ON CONFLICT DO NOTHING;
INSERT INTO public.groups (id, name)
VALUES (9, 'Sberbank')
ON CONFLICT DO NOTHING;
INSERT INTO public.groups (id, name)
VALUES (10, 'Ural')
ON CONFLICT DO NOTHING;
INSERT INTO public.groups (id, name)
VALUES (11, 'Office-12/25')
ON CONFLICT DO NOTHING;
INSERT INTO public.groups (id, name)
VALUES (12, 'Office-11/34')
ON CONFLICT DO NOTHING;
-- SELECT setval('groups_id_seq', 100, false);

-- INSERT INTO public.devices (serial_number, ip, hostname, mac, device_model, firmware_version, created_at, deleted_at, status,
--                             note)
-- VALUES ('SN00123', null, null, null, 4, '1.3.0', 1575266288, 5, 1, null);
INSERT INTO public.devices (id, serial_number, ip, hostname, mac, model_id, firmware_version, created_at,
                            deleted_at,
                            status, note)
VALUES (1, 'SN00222', '1.1.1.1', 'hostname1', '11:11:11:11:11:11', 5, '1.6.4', 1575266366, 6, 2, 'Note text')
ON CONFLICT DO NOTHING;
INSERT INTO public.devices (id, serial_number, ip, hostname, mac, model_id, firmware_version, created_at,
                            deleted_at,
                            status, note)
VALUES (2, 'SN00555', '5.5.5.5', 'host5', '55:55:55:55:55:55', 6, null, 1575266366, 7, 1, null)
ON CONFLICT DO NOTHING;
INSERT INTO public.devices (id, serial_number, ip, hostname, mac, model_id, firmware_version, created_at,
                            deleted_at,
                            status, note)
VALUES (3, 'SN0007777', '77.77.7.7', 'host.sn777', '77:77:77:77:77:77', 3, null, 1575266366, 8, 2, null)
ON CONFLICT DO NOTHING;
INSERT INTO public.devices (id, serial_number, ip, hostname, mac, model_id, firmware_version, created_at,
                            deleted_at,
                            status, note)
VALUES (4, 'ABX555', '255.255.255.254', 'last-device-ever', 'ff:ff:ff:ff:ff:ff', 1, null, 1575266366, 9, 3, null)
ON CONFLICT DO NOTHING;
INSERT INTO public.devices (id, serial_number, ip, hostname, mac, model_id, firmware_version, created_at,
                            deleted_at,
                            status, note)
VALUES (5, 'DIR999', '99.99.99.9', 'host99', '01:23:45:67:89:ab', 2, null, 1575266366, 10, 1, null)
ON CONFLICT DO NOTHING;
INSERT INTO public.devices (id, serial_number, ip, hostname, mac, model_id, firmware_version, created_at,
                            deleted_at,
                            status, note)
VALUES (6, 'REAL_ESR_10', '10.25.96.121', '10.25.96.121', '01:23:11:62:19:ba', 1, null, 1575266366, 10, 1, null)
ON CONFLICT DO NOTHING;
-- SELECT setval('devices_id_seq', 100, false);

INSERT INTO public.devices_groups (id, device_id, group_id)
VALUES (1, 3, 1)
ON CONFLICT DO NOTHING;
INSERT INTO public.devices_groups (id, device_id, group_id)
VALUES (2, 3, 2)
ON CONFLICT DO NOTHING;
INSERT INTO public.devices_groups (id, device_id, group_id)
VALUES (3, 3, 3)
ON CONFLICT DO NOTHING;
INSERT INTO public.devices_groups (id, device_id, group_id)
VALUES (4, 1, 4)
ON CONFLICT DO NOTHING;
INSERT INTO public.devices_groups (id, device_id, group_id)
VALUES (5, 2, 1)
ON CONFLICT DO NOTHING;
INSERT INTO public.devices_groups (id, device_id, group_id)
VALUES (6, 4, 1)
ON CONFLICT DO NOTHING;
-- SELECT setval('devices_groups_id_seq', 100, false);

INSERT INTO device_manager_users (id, username, password)
VALUES (1, 'admin', 'admin')
ON CONFLICT DO NOTHING;
INSERT INTO device_manager_users (id, username, password)
VALUES (2, 'admin', 'admin')
ON CONFLICT DO NOTHING;
INSERT INTO device_manager_users (id, username, password)
VALUES (3, 'admin', 'admin')
ON CONFLICT DO NOTHING;
INSERT INTO device_manager_users (id, username, password)
VALUES (4, 'admin', 'admin')
ON CONFLICT DO NOTHING;
INSERT INTO device_manager_users (id, username, password)
VALUES (5, 'admin', 'admin')
ON CONFLICT DO NOTHING;
INSERT INTO device_manager_users (id, username, password)
VALUES (6, 'admin', 'password1')
ON CONFLICT DO NOTHING;
-- SELECT setval('device_manager_users_id_seq', 100, false);
