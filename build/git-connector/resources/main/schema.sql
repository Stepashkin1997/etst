create table if not exists git_connector_template
(
    id bigserial not null
        constraint template_pk primary key,
    last_version bigint not null,
    deleted_at bigint null
);

create table if not exists git_connector_template_version
(
    id bigserial not null
        constraint template_meta_pk primary key,
    template_id bigint not null
        constraint template_id_fk
            references git_connector_template
             on update cascade on delete restrict,
    template_name varchar(255) not null,
    version bigint not null,
    username varchar(255) not null,
    annotation text not null,
    content_size bigint not null,
    addition_date bigint not null,
    is_zero bool not null,
    commit_hash varchar(41) null
);

create table if not exists git_connector_template_to_model
(
    id bigserial not null
        constraint template_to_model_pk primary key,
    template_id bigint not null
        constraint template_id_fk
            references git_connector_template
             on update cascade on delete restrict,
    model_id bigint not null
);

create table if not exists git_connector_configs
(
    id bigserial not null
        constraint config_id primary key,
    config_name text not null,
    config_type int,   -- config types: draft (0), backup (1), running (2), unrecognized(-1)
    device_id bigint not null, -- device_id from device manager
    author text not null,
    annotation text,
    created_at bigint not null,
    content_size bigint null,
    commit_hash varchar(41) null,
    branch_name text null
);
