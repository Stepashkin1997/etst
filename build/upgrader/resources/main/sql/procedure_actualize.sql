CREATE OR REPLACE FUNCTION changeActual(model_id BIGSERIAL, firmwareId BIGSERIAL) RETURNS VOID AS $$
DECLARE
    test table(id BIGSERIAL, firmware_id BIGSERIAL, model_id BIGSERIAL, actual BOOLEAN);
BEGIN
    SELECT * FROM upgrader_firmware_models INTO test;

    for i in test LOOP
        if i.model_id = model_id then
            i.actual = false;
        end if;
        if i.firmware_id = firmwareId then
            i.actual = true;
        end if;
    end loop;
end;
$$
