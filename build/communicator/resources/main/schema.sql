CREATE TABLE IF NOT EXISTS communicator_tasks
(
    id         BIGSERIAL NOT NULL PRIMARY KEY,
    meta_id    BIGINT,
    token      TEXT,
    history_id BIGINT,
    started_at BIGINT,
    token_expired BOOLEAN
);

CREATE INDEX CONCURRENTLY IF NOT EXISTS meta_id_idx ON communicator_tasks (meta_id);
